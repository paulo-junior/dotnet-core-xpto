﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Xpto.Repository;

namespace Xpto.Repository.Migrations
{
    [DbContext(typeof(XptoDbContext))]
    partial class XptoDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.9");

            modelBuilder.Entity("Xpto.Domain.Usuario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("INTEGER");

                    b.Property<string>("CPF")
                        .IsRequired()
                        .HasColumnName("cpf")
                        .HasColumnType("TEXT")
                        .HasMaxLength(15);

                    b.Property<string>("DataNascimento")
                        .IsRequired()
                        .HasColumnName("dataNascimento")
                        .HasColumnType("TEXT")
                        .HasMaxLength(15);

                    b.Property<char>("Genero")
                        .HasColumnName("genero")
                        .HasColumnType("TEXT")
                        .HasMaxLength(1);

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasColumnType("TEXT")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("usuarios");
                });
#pragma warning restore 612, 618
        }
    }
}
