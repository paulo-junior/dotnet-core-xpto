using Microsoft.EntityFrameworkCore;
using Xpto.Domain;

namespace Xpto.Repository
{
    /// <summary>
    /// Classe responsável pelo mapeamento e configurações das entidades com o banco de dados através de herança da classe de contexto DbContext.
    /// </summary>
    public class XptoDbContext : DbContext
    {
        /// <value>Especifica a classe Usuario como uma entidade para o banco de dados.</value>
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"DataSource=xpto.db;");
        }


        /// <summary>
        /// Método sobrescrito para definição de um modelo de tabela e campos referentes a entidade Usuario no banco de dados.
        /// </summary>
        /// <param name="modelBuilder">Propriedade de atribuição para um modelo de entidade do banco de dados.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>((builder) =>
            {
                builder.ToTable("usuarios");
                builder.Property(u => u.Id).HasColumnName("id");
                builder.HasKey(u => u.Id);
                builder.Property(u => u.Nome).HasColumnName("nome").HasMaxLength(50).IsRequired();
                builder.Property(u => u.CPF).HasColumnName("cpf").HasMaxLength(15).IsRequired();
                builder.Property(u => u.DataNascimento).HasColumnName("dataNascimento").HasMaxLength(15).IsRequired();
                builder.Property(u => u.Genero).HasColumnName("genero").HasMaxLength(1).IsRequired();
            });
        }

    }
}
