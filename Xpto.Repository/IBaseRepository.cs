using System.Collections.Generic;

namespace Xpto.Repository
{
    /// <summary>
    /// Interface responsável pela atribuição de métodos génericos para manipulação de registros.
    /// </summary>
    /// <typeparam name="T">Representação génerica para entidade.</typeparam>
    /// <typeparam name="TC">Representação génerica para propriedade.</typeparam>
    public interface IBaseRepository<T, TC> where T : class
    {
        /// <summary>
        /// Método para selecionar todos os registros.
        /// </summary>
        /// <returns>Lista de registros referente a entidade.</returns>
        IEnumerable<T> SelecionarTodos();

        /// <summary>
        /// Método para inserir registro.
        /// </summary>
        /// <param name="entidade">Define uma entidade para inserção do registro.</param>
        void Inserir(T entidade);

        /// <summary>
        /// Método para selecionar registro por id.
        /// </summary>
        /// <param name="id">Parâmetro de seleção de registro.</param>
        // void SelecionarPorId(TC id);

        /// <summary>
        /// Método para atualizar registro por id.
        /// </summary>
        /// <param name="id">Parâmetro de atualização de registro.</param>
        // void AtualizarPorId(TC id);

        /// <summary>
        /// Método para excluir registro por id.
        /// </summary>
        /// <param name="id">Parâmetro de exclusão de registro.</param>
        // void ExcluirPorId(TC id);
    }
}
