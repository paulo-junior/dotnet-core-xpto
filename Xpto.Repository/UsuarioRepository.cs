using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xpto.Domain;

namespace Xpto.Repository
{
    /// <summary>
    /// Classe responsável por receber implementação da interface IBaseRepository.
    /// </summary>
    /// <remarks>
    /// Essa classe define os métodos para manipulação de registros referentes a entidade Usuario.
    /// </remarks>
    public class UsuarioRepository : IBaseRepository<Usuario, int>
    {
        XptoDbContext _context = new XptoDbContext();

        public IEnumerable<Usuario> SelecionarTodos()
        {
            Console.WriteLine("\n***** LISTA DE USUÁRIOS *****");
            IEnumerable<Usuario> usuarios = from u in _context.Usuarios select u;
            try
            {
                foreach (var usr in usuarios)
                {
                    Console.WriteLine($"\n[Id: {usr.Id} Nome: {usr.Nome} CPF: {usr.CPF} Data de Nascimento: {usr.DataNascimento} Género: {usr.Genero}]");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Erro ao listar Usuários: {ex.Message}\n");
            }
            return usuarios;
        }

        public void Inserir(Usuario usuario)
        {
            try
            {
                _context.Usuarios.Add(usuario);
                _context.SaveChanges();
                Console.WriteLine($"***** Usuário(a) '{usuario.Nome}' foi inserido(a) com sucesso! *****");
            }
            catch (System.Exception ex)
            {
                Console.WriteLine($"Erro ao inserir Usuário: {ex.Message}");
            }
        }
    }
}
