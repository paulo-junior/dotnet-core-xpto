using System.IO;
using NUnit.Framework;
using Xpto.App.Arquivo;

namespace Xpto.Tests
{
    [TestFixture]
    public class BaseArquivoTest
    {
        [Test]
        public void LerArquivo_DeveRetornarVerdeiroLeituraDeArquivo()
        {
            var conteudoDoArquivo = "{\"Nome\":\"Paulo Alves\",\"CPF\":\"208.624.680-35\",\"DataNascimento\":\"01/05/1985\",\"Genero\":\"M\"}";
            var caminhoDoArquivo = @"C:\dev\ws-dotnet\dotnet-core-xpto\Xpto.Tests\BaseArquivoTestes\usuario-in.json";

            StreamWriter arquivoCriado;

            arquivoCriado = File.CreateText(caminhoDoArquivo);

            arquivoCriado.WriteLine(conteudoDoArquivo);

            arquivoCriado.Close();

            var baseArquivo = new BaseArquivo();
            var valorRecebido = baseArquivo.LerArquivo(caminhoDoArquivo);

            var valorEsperado = File.ReadAllText(@"C:\dev\ws-dotnet\dotnet-core-xpto\Xpto.Tests\BaseArquivoTestes\usuario-in.json");

            Assert.AreEqual(valorEsperado, valorRecebido);
        }
    }
}
