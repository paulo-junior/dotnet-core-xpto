using NUnit.Framework;
using Xpto.Domain;

namespace Xpto.Tests
{
    [TestFixture]
    public class UsuarioTest
    {
        /// <summary>
        /// Método para testar o preenchimento do campo 'Nome' de acordo com a implementação do método privado 'ValidarCamposVazios()',
        /// com chamada feita pelo método 'ValidarCampos()'. Caso o campo seja vázio o teste é aprovado.
        /// </summary>
        [Test]
        public void ValidarCampos_DeveRetornarFalsoComNomeVazio()
        {
            var usuario = new Usuario("", "208.624.680-35", "01/05/1985", 'M');
            Assert.IsFalse(usuario.ValidarCampos());
        }

        /// <summary>
        /// Método para testar o preenchimento do campo 'CPF' de acordo com a implementação do método privado 'ValidarCamposVazios()',
        /// com chamada feita pelo método 'ValidarCampos()'. Caso o campo seja vázio o teste é aprovado.
        /// </summary>
        [Test]
        public void ValidarCampos_DeveRetornarFalsoComCPFVazio()
        {
            var usuario = new Usuario("Paulo Alves", "", "01/05/1985", 'M');
            Assert.IsFalse(usuario.ValidarCampos());
        }

        /// <summary>
        /// Método para testar o preenchimento do campo 'DataNascimento' de acordo com a implementação do método privado 'ValidarCamposVazios()',
        /// com chamada feita pelo método 'ValidarCampos()'. Caso o campo seja vázio o teste é aprovado.
        /// </summary>
        [Test]
        public void ValidarCampos_DeveRetornarFalsoComDataNascimentoVazia()
        {
            var usuario = new Usuario("Paulo Alves", "208.624.680-35", "", 'M');
            Assert.IsFalse(usuario.ValidarCampos());
        }

        /// <summary>
        /// Método para testar o prenchimento de todos os campos de acordo com a implementação do método privado 'ValidarCamposVazios()', 
        /// com chamada feita pelo método 'ValidarCampos()'. Caso todos os campos sejam preenchidos o teste é aprovado.
        /// </summary>
        [Test]
        public void ValidarCampos_DeveRetornarVerdadeiroComTodosOsCamposPreenchidos()
        {
            var usuario = new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", 'M');
            Assert.IsTrue(usuario.ValidarCampos());
        }

        /// <summary>
        /// Método para testar o valor do campo 'Genero' de acordo com a implementação do método privado 'ValidarTipoGenero()',
        /// com chamada feita pelo método 'ValidarCampos()'. Se o valor recebido for diferente de 'M' masculino, 'F' feminino e 'I' indiferente 
        /// o teste confirma o valor como aprovado.
        /// </summary>
        /// <param name="genero">Parâmetro a ser testado.</param>
        /// <returns>Parâmetro gênero validado como indefinido.</returns>
        [TestCase('M', ExpectedResult = true)]
        [TestCase('F', ExpectedResult = true)]
        [TestCase('I', ExpectedResult = true)]
        [TestCase('D', ExpectedResult = false)]
        public bool ValidarCampos_DeveConfirmarGeneroIndefinido(char genero)
        {
            var usuario = new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", genero);
            return usuario.ValidarCampos();
        }

        /// <summary>
        /// Método para testar o valor do campo 'Nome' de acordo com a implementação do método privado 'ValidarNomeSobrenome()',
        /// com chamada feita pelo método 'ValidarCampos()'. Se o valor recebido for apenas o nome sem sobrenome o teste confirma nome como aprovado.
        /// </summary>
        /// <param name="nome">Parâmetro a ser testado.</param>
        /// <returns>Parâmetro nome validado como incompleto.</returns>
        [TestCase("Paulo Alves", ExpectedResult = true)]
        [TestCase("Paulo Alves Monteiro", ExpectedResult = true)]
        [TestCase("Paulo", ExpectedResult = false)]
        [TestCase("PauloAlvesMonteiro", ExpectedResult = false)]
        public bool ValidarCampos_DeveConfirmarNomeIncompleto(string nome)
        {
            var usuario = new Usuario(nome, "208.624.680-35", "01/05/1985", 'M');
            return usuario.ValidarCampos();
        }

        /// <summary>
        /// Método para testar o valor do campo 'DataNascimento' de acordo com a implementação do método privado 'ValidarDataNascimento()', 
        /// com chamada feita pelo método 'ValidarCampos()'. Se o formato do valor recebido for diferente de dia/mês/ano 
        /// ou for atribuida uma data inexistente o teste confirma data inválida como aprovada.
        /// </summary>
        /// <param name="dataNascimento">Parâmetro a ser testado.</param>
        /// <returns>Parâmetro nome validado como formato incorreto.</returns>
        [TestCase("01/05/1985", ExpectedResult = true)]
        [TestCase("01051985", ExpectedResult = false)]
        [TestCase("32/13/9999", ExpectedResult = false)]
        public bool ValidarCampos_DeveConfirmarDataNascimentoInvalida(string dataNascimento)
        {
            var usuario = new Usuario("Paulo Alves", "208.624.680-35", dataNascimento, 'M');
            return usuario.ValidarCampos();
        }

    }
}
