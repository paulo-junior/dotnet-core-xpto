using System.Collections.Generic;
using NUnit.Framework;
using Xpto.App.Utils;
using Xpto.Domain;

namespace Xpto.Tests
{
    [TestFixture]
    public class UsuarioUtilsTest
    {
        /// <summary>
        /// Método para testar se lista de usuários possui nomes repetidos de acordo com a validação do método ValidarNomesRepetidosListaDeUsuarios(). 
        /// Se na lista constar nomes repetidos o teste deve retornar falso.
        /// </summary>
        [Test]
        public void ValidarNomesRepetidosListaDeUsuarios_DeveRetornarFalsoComNomeRepetido()
        {
            UsuarioUtils usuarioUtils = new UsuarioUtils();
            var listaDeUsuariosTest = new List<Usuario>();

            listaDeUsuariosTest.Add(new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", 'M'));
            listaDeUsuariosTest.Add(new Usuario("Isadora Ferreira", "077.035.350-00", "14/09/1982", 'F'));
            listaDeUsuariosTest.Add(new Usuario("Joana Santos", "059.037.491-88", "22/05/1987", 'F'));
            listaDeUsuariosTest.Add(new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", 'M'));

            Assert.IsFalse(usuarioUtils.ValidarNomesRepetidosListaDeUsuarios(listaDeUsuariosTest));
        }

        /// <summary>
        /// Método para testar se lista de usuários possui nomes repetidos de acordo com a validação do método ValidarNomesRepetidosListaDeUsuarios(). 
        /// Se na lista não constar nomes repetidos o teste deve retornar verdadeiro.
        /// </summary>
        [Test]
        public void ValidarNomesRepetidosListaDeUsuarios_DeveRetornarVerdadeiroSemNomeRepetido()
        {
            UsuarioUtils usuarioUtils = new UsuarioUtils();
            var listaDeUsuariosTest = new List<Usuario>();

            listaDeUsuariosTest.Add(new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", 'M'));
            listaDeUsuariosTest.Add(new Usuario("Isadora Ferreira", "077.035.350-00", "14/09/1982", 'F'));
            listaDeUsuariosTest.Add(new Usuario("Joana Santos", "059.037.491-88", "22/05/1987", 'F'));
            listaDeUsuariosTest.Add(new Usuario("Paulo Alve", "208.624.680-35", "01/05/1985", 'M'));

            Assert.IsTrue(usuarioUtils.ValidarNomesRepetidosListaDeUsuarios(listaDeUsuariosTest));
        }
    }
}
