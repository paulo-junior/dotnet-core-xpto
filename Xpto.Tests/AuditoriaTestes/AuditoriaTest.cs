using System;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using Moq;
using NUnit.Framework;
using Xpto.App.AuditoriaUtil;

namespace Xpto.Tests.AuditoriaTestes
{
    [TestFixture]
    public class AuditoriaTest
    {
        [Test]
        public void InserirMensagem_DeveRetornarVerdadeiroConteudoDoArquivo()
        {
            var caminhoDoArquivo = "msg-auditoria-teste.txt";
            var titulo = "Arquivo de auditoria";

            var data = new DateTime(2021, 03, 23);
            var dataRecebida = data.ToString();

            var conteudoDoArquivo = $"Título: {titulo} \nData da Auditoria: {dataRecebida}";

            var caminhoMock = Mock.Of<IFile>(a => a.ReadAllText(caminhoDoArquivo) == conteudoDoArquivo);
            var dataMock = Mock.Of<IData>(d => d.DataAtual() == data);

            var auditoriaMock = new Auditoria(caminhoMock, dataMock);

            var dataMockAtual = dataMock.DataAtual().ToString();

            var valorRecebido = auditoriaMock.InserirMensagem(caminhoDoArquivo, titulo);

            Assert.AreEqual(conteudoDoArquivo, valorRecebido);
        }
    }
}
