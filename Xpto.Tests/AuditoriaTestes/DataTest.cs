using System;
using NUnit.Framework;
using Xpto.App.AuditoriaUtil;

namespace Xpto.Tests.AuditoriaTestes
{
    [TestFixture]
    public class DataTest
    {
        [Test]
        public void DataAtual_DeveRetornarVerdadeiroDataAtual()
        {
            var data = new Data();

            var dataRetorno = data.DataAtual();

            var dataEsperada = DateTime.Now;

            var intervaloDeTempo = dataEsperada - dataRetorno;

            Assert.IsTrue(intervaloDeTempo.TotalSeconds < 1 && intervaloDeTempo.TotalMilliseconds > 0);

        }
    }
}
