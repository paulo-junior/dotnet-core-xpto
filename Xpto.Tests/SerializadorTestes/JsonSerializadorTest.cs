using NUnit.Framework;
using Xpto.App.Serializacao;
using Xpto.Domain;

namespace Xpto.Tests
{
    [TestFixture]
    public class JsonSerializadorTest
    {
        /// <summary>
        /// Método para testar se retorna como verdadeira a serialização de uma instância do objeto usuário através do método Serializar().
        /// </summary>
        [Test]
        public void Serializar_DeveRetornarVerdadeiroUsuarioSerializado()
        {
            var usuario = new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", 'M');

            var jsonSerializador = new JsonSerializador();

            var valorRecebido = jsonSerializador.Serializar<Usuario>(usuario);

            var valorEsperado = "{\"Id\":0,\"Nome\":\"Paulo Alves\",\"CPF\":\"208.624.680-35\",\"DataNascimento\":\"01/05/1985\",\"Genero\":\"M\"}";

            Assert.AreEqual(valorEsperado, valorRecebido);
        }

        [Test]
        public void Deserializar_DeveRetornarVerdadeiroUsuarioDeserializado()
        {
            var jsonSerializador = new JsonSerializador();

            var json = "{\"Id\":0,\"Nome\":\"Paulo Alves\",\"CPF\":\"208.624.680-35\",\"DataNascimento\":\"01/05/1985\",\"Genero\":\"M\"}";

            var valorRecebido = jsonSerializador.Deserializar<Usuario>(json);

            var nomeRecebido = valorRecebido.Nome;
            var CPFRecebido = valorRecebido.CPF;
            var dataRecebida = valorRecebido.DataNascimento;
            var generoRecebido = valorRecebido.Genero;

            var valorEsperado = new Usuario("Paulo Alves", "208.624.680-35", "01/05/1985", 'M');

            var nomeEsperado = valorEsperado.Nome;
            var CPFEsperado = valorEsperado.CPF;
            var dataEsperada = valorEsperado.DataNascimento;
            var generoEsperado = valorEsperado.Genero;

            Assert.AreEqual(nomeEsperado, nomeRecebido);
            Assert.AreEqual(CPFEsperado, CPFRecebido);
            Assert.AreEqual(dataEsperada, dataRecebida);
            Assert.AreEqual(generoEsperado, generoRecebido);
        }
    }
}
