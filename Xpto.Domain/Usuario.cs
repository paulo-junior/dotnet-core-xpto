using System;
using System.Text.RegularExpressions;

namespace Xpto.Domain
{
    /// <summary>
    /// Classe responsável pela definição das propriedades referentes a entidade Usuario.
    /// </summary>
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string DataNascimento { get; set; }
        public char Genero { get; set; }

        public Usuario(string nome, string cpf, string dataNascimento, char genero)
        {
            Nome = nome;
            CPF = cpf;
            DataNascimento = dataNascimento;
            Genero = genero;
        }

        public override string ToString()
        {
            return $"\nNome: {Nome} \nCPF: {CPF} \nData de Nascimento: {DataNascimento} \nGênero: {Genero}";
        }

        /// <summary>
        /// Método responsável pela chamada de métodos privados relacionado as validações do usuário.
        /// </summary>
        /// <returns>Retorna a chamada dos métodos de validações.</returns>
        public bool ValidarCampos() => ValidarCamposVazios() && ValidarTipoGenero() && ValidarNomeSobrenome() && ValidarDataNascimento();

        /// <summary>
        /// Método para validar o preenchimento dos campos do Usuario.
        /// </summary>
        /// <param name="usuario">Define uma instância do Usuario a ser validado.</param>
        /// <returns>Propriedades do Usuario validado.</returns>
        private bool ValidarCamposVazios()
        {
            var msgCampoObrigatorio = " um campo obrigatório!";

            if (string.IsNullOrEmpty(Nome))
            {
                Console.WriteLine($"O Nome é {msgCampoObrigatorio}");
                return false;
            }

            if (string.IsNullOrEmpty(CPF))
            {
                Console.WriteLine($"O CPF é {msgCampoObrigatorio}");
                return false;
            }

            if (string.IsNullOrEmpty(DataNascimento))
            {
                Console.WriteLine($"A Data de Nascimento é {msgCampoObrigatorio}");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Método para validar o gênero informado.
        /// </summary>
        /// <example>
        /// 'M' para masculino - 'F' para feminino - 'I' para indiferente.
        /// </example>
        /// <param name="genero">Parâmetro a ser validado.</param>
        /// <returns>Validação da propriedade gênero.</returns>
        private bool ValidarTipoGenero()
        {
            var msgCampoGenero = "O Gênero tem ser uma das opções 'M' - 'F' - 'I'";

            if (this.Genero != 'M' && this.Genero != 'F' && this.Genero != 'I')
            {
                Console.WriteLine(msgCampoGenero);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Método para validar se nome do Usuario vem dividido com 'Nome' e 'Sobrenome'.
        /// </summary>
        /// <param name="nome">Parâmetro a ser validado.</param>
        /// <returns>Validação da propriedade nome.</returns>
        private bool ValidarNomeSobrenome()
        {
            var msgCampoNome = "Você precisa digitar 'Nome' e 'Sobrenome'!";

            Match match = Regex.Match(Nome, @"[A-Z][a-z]* [A-Z][a-z]*");
            if (!match.Success)
            {
                Console.WriteLine(msgCampoNome);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Método para verificar se a data informada pelo usuário é válida.
        /// </summary>
        /// <param name="dataNascimento">Parâmetro a ser validado.</param>
        /// <returns>Validação da propriedade Data de Nascimento.</returns>
        private bool ValidarDataNascimento()
        {
            var msgCampoDataNascimento = "Você precisa digitar uma data válida! Exemplo: dia/mês/ano - 01/12/1970";

            if (!DateTime.TryParse(DataNascimento, out DateTime dateTime))
            {
                Console.WriteLine(msgCampoDataNascimento);
                return false;
            }

            return true;
        }
    }
}
