namespace Xpto.App.AuditoriaUtil
{
    public interface IAuditoria
    {
        /// <summary>
        /// Método responsável pela criação de arquivo com mensagem sobre auditoria.
        /// </summary>
        /// <param name="caminho">Define o caminho do arquivo.</param>
        /// <param name="valor">Define o título do arquivo.</param>
        /// <returns>Texto com o conteúdo de mensagem sobre auditoria.</returns>
        public string InserirMensagem(string caminho, string valor);
    }
}
