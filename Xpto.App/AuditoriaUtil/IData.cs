using System;

namespace Xpto.App.AuditoriaUtil
{
    public interface IData
    {
        /// <summary>
        /// Método responsável pela definição de data e hora atual.
        /// </summary>
        /// <returns>A data e hora atual do sistema.</returns>
        DateTime DataAtual();
    }
}
