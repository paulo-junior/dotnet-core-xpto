using System;
using System.IO;
using System.IO.Abstractions;

namespace Xpto.App.AuditoriaUtil
{
    public class Auditoria : IAuditoria
    {
        private readonly IFile _fileSystem;

        private readonly IData _data;

        public Auditoria(IFile fileSystem, IData data)
        {
            _fileSystem = fileSystem;
            _data = data;
        }

        public string InserirMensagem(string caminho, string titulo)
        {
            StreamWriter arquivoAuditoria;
            arquivoAuditoria = File.CreateText(caminho);

            var dataAuditoria = _data.DataAtual().ToString();

            arquivoAuditoria.Write("***** Informações do arquivo recebido *****"
                                   + "\nTítulo: " + titulo
                                   + "\nData da Auditoria: " + dataAuditoria);

            arquivoAuditoria.Close();

            Console.WriteLine("Arquivo de auditoria criado!!!");

            var resultado = $"Título: {titulo} \nData da Auditoria: {dataAuditoria}";

            Console.WriteLine(resultado);

            return resultado;
        }
    }
}
