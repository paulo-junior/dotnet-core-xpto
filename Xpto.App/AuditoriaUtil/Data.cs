using System;

namespace Xpto.App.AuditoriaUtil
{
    public class Data : IData
    {
        public DateTime DataAtual() => DateTime.Now;
    }
}
