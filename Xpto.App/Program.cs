﻿using Xpto.App.Serializacao;
using Xpto.Domain;
using Xpto.Repository;
using Microsoft.Extensions.DependencyInjection;
using Xpto.App.Arquivo;
using Xpto.App.Servicos;
using Xpto.App.AuditoriaUtil;
using System.IO.Abstractions;

namespace Xpto.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
            .AddSingleton<IFileSystem, FileSystem>()
            .AddSingleton<IFile, FileWrapper>()
            .AddSingleton<IData, Data>()
            .AddSingleton<IBaseArquivo, Arquivo.BaseArquivo>()
            .AddSingleton<ISerializador, JsonSerializador>()
            .AddSingleton<IBaseRepository<Usuario, int>, UsuarioRepository>()
            .AddSingleton<IAuditoria, Auditoria>()
            .AddSingleton<Servico>()
            .BuildServiceProvider();

            var service = serviceProvider.GetService<Servico>();
            service.Executar("usuarios.json");
        }
    }
}
