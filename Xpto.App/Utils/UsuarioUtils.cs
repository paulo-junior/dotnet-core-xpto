using System;
using System.Collections.Generic;
using System.Linq;
using Xpto.Domain;

namespace Xpto.App.Utils
{
    /// <summary>
    /// Classe responsável pelo uso de métodos utilitários referente a entidade Usuario.
    /// </summary>
    public class UsuarioUtils
    {
        /// <summary>
        /// Método para validar se lista de usuários possui nomes repetidos.
        /// </summary>
        /// <param name="listaDeUsuarios">Lista de usuário para validação.</param>
        /// <returns>Validação dos elementos da lista.</returns>
        public bool ValidarNomesRepetidosListaDeUsuarios(List<Usuario> listaDeUsuarios)
        {
            try
            {
                var qtdNomesDuplicados = listaDeUsuarios
                .GroupBy(u => new { u.Nome })
                .Where(u => u.Count() > 1);

                if (qtdNomesDuplicados.Any())
                {
                    Console.WriteLine($"A lista possui '{qtdNomesDuplicados.Count()}' nome(s) duplicado(s). Não é permitido duplicidade!");
                    return false;
                }
                else
                {
                    foreach (var listaUsuarios in listaDeUsuarios)
                    {
                        Console.WriteLine(listaUsuarios);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Erro ao validar a lista de usuários: {ex.Message}");
            }

            return true;
        }
    }
}
