namespace Xpto.App.Arquivo
{
    /// <summary>
    /// Interface responsável pela leitura de arquivo.
    /// </summary>
    public interface IBaseArquivo
    {
        /// <summary>
        /// Método responsável por obter leitira de arquivo fornecido por parâmetro.
        /// </summary>
        /// <param name="caminho">Parâmetro que indica caminho do arquivo para leitura.</param>
        /// <returns>Conteúdo do arquivo em forma de string.</returns>
        public string LerArquivo(string caminho);
    }
}
