using System.IO;

namespace Xpto.App.Arquivo
{
    /// <summary>
    /// Classe responsável por leitura de arquivo a partir da implementação da interface IBaseArquivo.
    /// </summary>
    public class BaseArquivo : IBaseArquivo
    {
        public string LerArquivo(string caminho)
        {
            return File.ReadAllText(caminho);
        }
    }
}
