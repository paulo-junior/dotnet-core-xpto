namespace Xpto.App.Serializacao
{
    /// <summary>
    /// Classe responsável por implemetar serialização e deserialização de objeto em Json.
    /// </summary>
    public class JsonSerializador : ISerializador
    {
        public string Serializar<T>(T usuario)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(usuario);
            return json;
        }

        public T Deserializar<T>(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json); 
        }
    }
}