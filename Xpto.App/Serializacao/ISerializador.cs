namespace Xpto.App.Serializacao
{
    /// <summary>
    /// Interface responsável pela serialização e deserialização de objetos.
    /// </summary>
    public interface ISerializador
    {
        /// <summary>
        /// Método responsável pela serialização de objeto.
        /// </summary>
        /// <param name="entrada">Define uma entrada de tipo de parâmetro.</param>
        /// <typeparam name="T">Define um tipo de objeto.</typeparam>
        /// <returns>String serializada.</returns>
        string Serializar<T>(T entrada);

        /// <summary>
        /// Método responsável pela deserialização de objeto.
        /// </summary>
        /// <param name="entrada">Define uma entrada de tipo de parâmetro.</param>
        /// <typeparam name="T">Define um tipo de objeto.</typeparam>
        /// <returns>Objeto deserializado.</returns>
        T Deserializar<T>(string entrada);
    }
}