using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xpto.App.Arquivo;
using Xpto.App.AuditoriaUtil;
using Xpto.App.Serializacao;
using Xpto.App.Utils;
using Xpto.Domain;
using Xpto.Repository;

namespace Xpto.App.Servicos
{
    /// <summary>
    /// Classe responsável pela implementação de serviços utilizados na execução da aplicação.
    /// </summary>
    public class Servico
    {
        private readonly IBaseArquivo _arquivo;
        private readonly ISerializador _serializador;
        private readonly IBaseRepository<Usuario, int> _repository;
        private readonly IAuditoria _auditoria;
        UsuarioUtils _util = new UsuarioUtils();

        public Servico(IBaseArquivo arquivo, ISerializador serializador, IBaseRepository<Usuario, int> repository, IAuditoria auditoria)
        {
            _arquivo = arquivo;
            _serializador = serializador;
            _repository = repository;
            _auditoria = auditoria;
        }

        /// <summary>
        /// Método responsável por executar serviços de leitura de arquivo com lista de usuários, validações e adição da lista de usuários no banco de dados.
        /// </summary>
        /// <param name="caminho">Representa o arquivo de leitura da lista de usuários.</param>
        public void Executar(string caminho)
        {
            List<Usuario> listaDeUsuariosLeitura = AdquirindoListaDeUsuariosNoArquivo(caminho);

            if (listaDeUsuariosLeitura.Any(u => !u.ValidarCampos()))
            {
                Console.WriteLine("Erro, a lista de usuário possui dados inválidos!");
                return;
            }

            if (!_util.ValidarNomesRepetidosListaDeUsuarios(listaDeUsuariosLeitura))
            {
                Console.WriteLine("Erro, a lista de usuários possui nomes iguais!");
                return;
            }

            foreach (var item in listaDeUsuariosLeitura)
            {
                _repository.Inserir(item);
            }

            _auditoria.InserirMensagem(@"AuditoriaUtil\msg-auditoria.txt", "Auditoria Março!!!");

        }

        /// <summary>
        /// Método responsável pela leitura do arquivo que contém a lista de usuários.
        /// </summary>
        /// <param name="caminho">Representa o arquivo de leitura da lista de usuários.</param>
        /// <returns>Lista de usuários.</returns>
        private List<Usuario> AdquirindoListaDeUsuariosNoArquivo(string caminho)
        {
            var json = _arquivo.LerArquivo(caminho);

            Console.WriteLine("***** LISTA DE USUÁRIOS *****");
            var listaDeUsuarios = _serializador.Deserializar<List<Usuario>>(json);

            Console.WriteLine(json);

            return listaDeUsuarios;
        }
    }
}
