## Projeto XPTO

Projeto do tipo Console Application para fazer leitura de um arquivo no formato Json e tratar de uma lista de usuários com as seguintes propriedades: Nome, CPF, Data de nascimento e Gênero.

### Objetivo

Validar as propriedades para que todas estejam preenchidas e que não tenha nenhum documento igual, que o gênero e a data de nascimento sejam válidas que tenha pelo menos dois nomes. EX: Vinicius - invalido; Vinicius Monteiro - válido após as validações, inserir no banco de dados, e em seguida criar testes de unidade para a aplicação.

### Recursos Utilizados

- Visual Studio Code
- Linguagem C#
- .NET Core 3.1
- Entity Framework Core
- SQLite

### Clonar repositório

`git clone https://paulo-junior@bitbucket.org/paulo-junior/dotnet-core-xpto.git`
